# zig

Imperative, general-purpose, statically typed, compiled system programming language. https://ziglang.org

* [Zig_(programming_language)](https://en.m.wikipedia.org/wiki/Zig_(programming_language))
* [ziglang.org/learn](https://ziglang.org/learn)
* [zig.guide](https://zig.guide)
* [repology.org...zig](https://repology.org/project/zig)
